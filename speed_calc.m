function [ speed ] = speed_calc( trajectory )

noSpots = size(trajectory,1)-1;

oneDdisplacements = trajectory(2:(noSpots+1),:) - trajectory(1:noSpots,:);
twoDdisplacements = sqrt( oneDdisplacements(:,1).^2 + ...
                          oneDdisplacements(:,2).^2 );
                      
speed = sum(twoDdisplacements,1) / noSpots;

end