%% 19 November 2015
%% Carl Wivagg
%% mm_cell_width
%
% This function takes a n-by-2 vector of integer coordinates representing
% the perimeter of a cell and returns the mean width of the cell in pixels.
% The mean width in this case is interpreted to mean the width around the
% major axis of the best-fitting ellipse.
%
% One acceptable input is the perimeter given by a Morphometrics data
% object, [ frame(i).object(j).Xperim, frame(i).object(j).Yperim ].

function [ width ] = mm_cell_width( perim )

xmin = min(perim(:,1))-1;
ymin = min(perim(:,2))-1;
xmax = max(perim(:,1));
ymax = max(perim(:,2));

noperims = size(perim,1);

perim = perim - ones(noperims,2) * [xmin,0;0,ymin;];
im = zeros(ymax+2,xmax+2);
for i = 1:noperims
    im(perim(i,2)+1,perim(i,1)+1) = 1;
end

im = imfill(im,'holes');
or = regionprops(im,'Orientation');
im = imrotate(im,-1*or(1).Orientation);

widths = sum(im);

nzwidths = widths(1,widths(1,:)~=0);
width = mean(nzwidths,2);

end