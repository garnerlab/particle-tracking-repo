%% 19 November 2015
%% Carl Wivagg
%% track_properties_analyzer
%
% Dependencies:
% This function requires these other functions to work.
%   mm_cell_width
%   wols       *WARNING needs validation
%   alpha_calc *WARNING needs validation
%   speed_calc
%   Ethan_MSD
%   Saman_PCA_Rfit
%   trackmate_xml_importer
%
% This function takes a TrackMate XML file and a Morphometrics CONTOURS.mat
% file and outputs various properties of the tracks in the TrackMate file.
% The function was designed to calculate the angle of the track to the cell
% midline, which is why the CONTOURS.mat file is required. Tracks whose
% center is in the five edge pixels of the image are discarded; this
% parameter is not changeable.
%
% This function outputs performance metrics:
%    Orphan tracks are tracks not matched to a cell.
%    Bounding tracks are those tracks in the five edge pixels.
%    Too fat tracks are those tracks matched to cells greater than the
%       width cutoff (20 pixels by default, see below).
% There is also a warning if a track is assigned to more than one cell.
% This should never happen, and if it does, the coder is to blame and
% something is seriously wrong with the function.
%
% Matching tracks to cells is a CPU-intensive process for amateur coders
% like me, so if properties relative to the cell are not desired, the user
% should choose a different function if possible.
%
% trackPropertiesObject =
%    track_properties_analyzer( trackFile, contoursFile )
%
% Specifying a cutoff for cells too large to be of interest is an optional
% additional argument. The default cutoff is 20.
%
% trackPropertiesObject =
%    track_properties_analyzer( trackFile, contoursFile, widthCutoff )
%
% WARNING
%  Additional arguments are necessary if the following assumptions are
%  deviated from:
% Image size is 1024 pixels.
% Pixel size is 15.432 pixels/um. NB pixel size is input in pixels/um;
%  properly, this is inverse pixel size.
%
% trackPropertiesObject = track_properties_analyzer(
%       trackFile,
%       contoursFile,
%       widthCutoff,
%       imageSize,
%       inversePixelSize )

function [ theseTrimmedTrackProperties ] = track_properties_analyzer_mrinal( ...
    trackFile, cellFile, MovieNumber, varargin )

if nargin < 6
    pixelsPerMicron = 15.432;
else
    pixelsPerMicron = varargin{3};
end
if nargin < 5
    pictureSize = 1024;
else
    pictureSize = varargin{2};
end
if nargin < 4
    cellWidthCutoff = 20;
else
    cellWidthCutoff = varargin{1};
end

pictureBorder = 5;

load(cellFile,'frame','Ncell');
% Overwrites theta_cont with cell width in CONTOURS.mat object.
for k = 1:Ncell
    frame(1).object(k).theta_cont = mm_cell_width( ...
        [ frame(1).object(k).Xperim, frame(1).object(k).Yperim ] );
end

theseTracks = trackmate_xml_importer_v2(trackFile);

% Initialization
Ntrack = size(theseTracks,2);
theseTrackProperties(1:Ntrack) = struct('center',zeros(2,1), ...
    'orientation',-3, ...
    'cell_within',-1, ...
    'midline_angle',-6, ...
    'alpha',-2.1, ...
    'speed', -1, ...
    'life', -1, ...
    'pos', [], ...
    'MSD_timestep', -10.2, ...
    'MSD_D', -5.7, ...
    'MSD_V', -2.3, ...
    'MSD_alpha', -2.3, ...
    'Rsq_log_log_MSD', -4.1, ...
    'Rfit', -1, ...
    'Movie', -66);
orphanTrackCounter = 0;
borderTrackCounter = 0;
toofatTrackCounter = 0;

% Track Properties Calculations
for k = 1:Ntrack
    
    % Translates TrackMate real locations to pixels.
    Xcoords = pixelsPerMicron * theseTracks(k).X(1:theseTracks(k).length);
    Ycoords = pixelsPerMicron * theseTracks(k).Y(1:theseTracks(k).length);
    
    % Eliminates tracks in border.
    centerX = int32(fix(mean(Xcoords)))+1;
    centerY = int32(fix(mean(Ycoords)))+1;
    if centerX < pictureBorder || centerY < pictureBorder || ...
            centerX > (pictureSize-pictureBorder) || ...
            centerY > (pictureSize-pictureBorder)
        borderTrackCounter = borderTrackCounter + 1;
        continue
    end
    
    % Locates the cell that a track belongs in.
    for l = 1:Ncell
        rOfLBound = 0; lOfRBound = 0; tOfBBound = 0; bOfTBound = 0;
        perimSize = size(frame(1).object(l).Xperim,1);
        for m = 1:perimSize
            if frame(1).object(l).Xperim(m) == centerX && ...
                    centerY >= frame(1).object(l).Yperim(m)
                tOfBBound = 1;
                break
            end
        end
        if tOfBBound == 0
            continue
        end
        for m = 1:perimSize
            if frame(1).object(l).Xperim(m) == centerX && ...
                    centerY <= frame(1).object(l).Yperim(m)
                bOfTBound = 1;
                break
            end
        end
        if bOfTBound == 0
            continue
        end
        for m = 1:perimSize
            if frame(1).object(l).Yperim(m) == centerY && ...
                    centerX >= frame(1).object(l).Xperim(m)
                rOfLBound = 1;
                break
            end
        end
        if rOfLBound == 0
            continue
        end
        for m = 1:perimSize
            if frame(1).object(l).Yperim(m) == centerY && ...
                    centerX <= frame(1).object(l).Xperim(m)
                lOfRBound = 1;
                break
            end
        end
        if rOfLBound == 1 && lOfRBound == 1 && ...
                tOfBBound == 1 && bOfTBound == 1
            if theseTrackProperties(k).cell_within ~= -1;
                disp('Multiple track assignment warning.');
            end
            
            theseTrackProperties(k).cell_within = l;
            break
        end
    end
    if theseTrackProperties(k).cell_within == -1
        orphanTrackCounter = orphanTrackCounter + 1;
    else
        % Discards tracks located in fat cells and cells with no pill mesh
        % and cells with a tiny pill mesh
        if ( frame(1).object(theseTrackProperties(k).cell_within).theta_cont ...
                > cellWidthCutoff ) || ...
            ( size(frame(1).object(theseTrackProperties(k).cell_within).pill_mesh,2) ~= 4 ) || ...
            ( size(frame(1).object(theseTrackProperties(k).cell_within).pill_mesh,1) < 3 )
            toofatTrackCounter = toofatTrackCounter + 1;
            theseTrackProperties(k).cell_within = -1;
            % Writes track properties for those cells passing all cutoffs.
        else
             theseTrackProperties(k).center(1,1) = mean(Xcoords);
            theseTrackProperties(k).center(2,1) = mean(Ycoords);
            [a, b, c] = wols(Xcoords,Ycoords,ones(size(Xcoords,1),1));
            theseTrackProperties(k).orientation = (180/pi)*atan( -1 * a / b );
            theseTrackProperties(k).alpha = alpha_calc([Xcoords,Ycoords]);
            theseTrackProperties(k).midline_angle = pillMeshAngleFinder( ...
                frame(1).object(theseTrackProperties(k).cell_within).pill_mesh, ...
                theseTrackProperties(k).orientation, ...
                theseTrackProperties(k).center);
            if theseTrackProperties(k).midline_angle > 90
                theseTrackProperties(k).midline_angle = ...
                    180 - theseTrackProperties(k).midline_angle;
            end
            if theseTrackProperties(k).midline_angle < -90
                theseTrackProperties(k).midline_angle = ...
                    -180 - theseTrackProperties(k).midline_angle;
            end
            theseTrackProperties(k).speed = 1000/pixelsPerMicron * ...
                speed_calc([Xcoords, Ycoords]);
            theseTrackProperties(k).life = size(Xcoords,1); %store lifetime of track in # of frames
            theseTrackProperties(k).pos = [Xcoords,Ycoords]; % store all the XY coordinates of the track
            
            % Orthogonal regression using PCA (adapted from Saman) 
            
            [theseTrackProperties(k).Rfit] = Saman_PCA_Rfit(theseTrackProperties(k).pos);
            
            % MSD vs time, V, D, alpha, MFit calculation (by Ethan's code)
            
            [theseTrackProperties(k).MSD_timestep, ...
                theseTrackProperties(k).MSD_D, ...
                theseTrackProperties(k).MSD_V, ...
                theseTrackProperties(k).MSD_alpha, ...
                theseTrackProperties(k).Rsq_log_log_MSD] = Ethan_MSD(theseTrackProperties(k).pos, pixelsPerMicron);
            
            % This is to keep track of which movie the current molecule came
            % from, which will be useful for batch processing
            
            theseTrackProperties(k).Movie = MovieNumber;
            
        end
    end
end

% Condenses output object to contain only objects for tracks within cells
% (i.e. excludes those tracks for whom 'cell_within' has remained the initial value of -1)
theseTrimmedTrackProperties = theseTrackProperties([theseTrackProperties.cell_within] ~= -1);

% Prints metrics.
metrics = ...
    sprintf('\nOrphans: %d\nBounding: %d\nToo fat: %d\nTotal: %d\n', ...
    orphanTrackCounter, borderTrackCounter, toofatTrackCounter, Ntrack);
disp(metrics);

end