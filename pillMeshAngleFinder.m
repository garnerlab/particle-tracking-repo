function [ relAngle ] = pillMeshAngleFinder( pillMesh, absAngle, center )

pillMesh = pillMesh(2:size(pillMesh,1)-1,:);
colCent = ones(size(pillMesh,1),2) * [center(1) 0; 0 center(2)];

distLine = abs( colCent(:,2).*(pillMesh(:,3)-pillMesh(:,1)) - ...
          colCent(:,1).*(pillMesh(:,4)-pillMesh(:,2)) + ...
          pillMesh(:,4).*pillMesh(:,1) - pillMesh(:,3).*pillMesh(:,2) ) ./ ...
        sqrt( (pillMesh(:,3)-pillMesh(:,1)).^2 + ...
              (pillMesh(:,4)-pillMesh(:,2)).^2 );

distPt1 = sqrt( (pillMesh(:,4)-colCent(:,2)).^2 + ...
                (pillMesh(:,3)-colCent(:,1)).^2 );
distPt2 = sqrt( (pillMesh(:,2)-colCent(:,2)).^2 + ...
                (pillMesh(:,1)-colCent(:,1)).^2 );

%lengthPillMesh = sqrt( (pillMesh(:,4)-pillMesh(:,2)).^2 + ...
%                       (pillMesh(:,3)-pillMesh(:,1)).^2 );
                   
validPillMesh = pillMesh(distLine<distPt1&distLine<distPt2,:);
validDist = distLine(distLine<distPt1&distLine<distPt2,1);

[M I] = min(validDist);
pillMeshRow = validPillMesh(I(1),:);

pillMeshAngle = 180/pi*atan((pillMeshRow(1,4)-pillMeshRow(1,2))/...
                (pillMeshRow(1,3)-pillMeshRow(1,1)));
            
relAngle = pillMeshAngle - absAngle;
