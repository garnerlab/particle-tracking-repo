%% 29 January 2016
%% Carl Wivagg
%% trackmate_xml_importer
%
% This version has been updated to include a new field, start, which is the
% first frame that a given track appears in.
%
% This function takes a .xml file exported from TrackMate and imports it
% into MATLAB. The file is imported as a structure array, with each object
% in the array representing one track. Each object has three fields:
% length, X, and Y.
% length is the length of the track.
% X is a list of the X coordinates the track goes through, 0 for indices
%    exceeding maxTrackLength.
% Y is a list of the Y coordinates the track goes through, 0 for indices
%    exceeding maxTrackLength.
%
% matlabObject = trackmate_xml_importer( fileName, maxTrackLength )
%
% maxTrackLength need not be specified. If it is not specified, it will
% assume a value of 100.
%
% Both the X and Y fields are vectors of length "maxTrackLength",
% representing the maximum expected track length. If a track exceeds the
% length, the function will throw an error. If a track is below the length,
% the remaining values will be zero.

function [ finalTracksObject ] = trackmate_xml_importer_v2( myFile, varargin )

if nargin < 2
    maxTrackLength = 100;
else
    maxTrackLength = varargin{1};
end

trackFile = xmlread(myFile);

allTracks = trackFile.getElementsByTagName('particle');
finalTracksObject(1:allTracks.getLength) = ...
    struct('length',0, ...
           'start',0, ...
           'X',zeros(maxTrackLength,1), ...
           'Y',zeros(maxTrackLength,1));
for k = 0:allTracks.getLength-1
    thisTrack = allTracks.item(k);
    spotsInTrack = thisTrack.getElementsByTagName('detection');
    thisSpot = spotsInTrack.item(0);
    finalTracksObject(k+1).start = str2double(thisSpot.getAttribute('t'))+1;
    finalTracksObject(k+1).length = spotsInTrack.getLength;
    for l = 0:spotsInTrack.getLength-1
        thisSpot = spotsInTrack.item(l);
        finalTracksObject(k+1).X(l+1,1) = ...
            str2double(thisSpot.getAttribute('x'));
        finalTracksObject(k+1).Y(l+1,1) = ...
            str2double(thisSpot.getAttribute('y'));
    end
end

end