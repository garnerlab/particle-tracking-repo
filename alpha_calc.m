function [ alpha ] = alpha_calc( track )

data = [ (1:size(track,1))',track];

nData = size(data,1); %# number of data points
%numberOfDeltaT = floor(nData/4); %# for MSD, dt should be up to 1/4 of number of data points
numberOfDeltaT = floor(nData-1);
%cw - blatantly disregarding this guideline

msd = zeros(numberOfDeltaT,1); %# We'll store [mean, std, n]

%# calculate msd for all deltaT's

for dt = 1:numberOfDeltaT
   deltaCoords = data(1+dt:end,2:3) - data(1:end-dt,2:3);
   squaredDisplacement = sum(deltaCoords.^2,2); %# dx^2+dy^2+dz^2

   msd(dt,1) = mean(squaredDisplacement); %# average
   %msd(dt,2) = std(squaredDisplacement); %# std
   %msd(dt,3) = length(squaredDisplacement); %# n
end

p = polyfit(log(1:numberOfDeltaT)',log(msd),1);
alpha = p(1);

end