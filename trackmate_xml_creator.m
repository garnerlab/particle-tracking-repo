%% 11 December 2015
%% Carl Wivagg
%% trackmate_xml_creator
%
% This function tracks particle movement in a .tif stack using the versions
% of FIJI and TrackMate present on your machine. It provides defaults for
% the many TrackMate parameters.
%
% trackmate_xml_creator( tifName, xmlName )
%
% The .xml file created will NOT be instantiable in TrackMate. It is the
% simplified .xml file created by navigating to the final pane of a
% TrackMate instance and selecting the "Export Tracks to XML" action. The
% only known use of such a file is for import into MATLAB using the
% trackmate_xml_importer function. At present, the function only allows you
% to track using the LoG detector, with subpixel localization, without
% median filtering, using a spot quality threshold, the simple LAP tracker
% with no gap closing, and filtering tracks using a minimum track length.
% Future versions or versions upon request may incorporate other options;
% once the TrackMate source code is understood, it is not terribly
% difficult to create variations.
%
% Detailed inputs:
%
% trackmate_xml_creator( tifName, xmlName, spotSize, background,
%     spotQualityThreshold, maximumLinkingDistance, minimumTrackLength )
%
% spotSize - This parameter is the "Estimated blob diameter" in the third
%     pane of TrackMate when using the LoG detector

% background - This parameter is the "Threshold" in the third pane of
%     TrackMate when using the LoG detector

% spotQualityThreshold - This parameter sets a minimum spot quality, spots
%     below which minimum are discarded, as in the fifth pane of TrackMate.
%     See TrackMate documentation.
%
% maximumLinkingDistance - This parameter sets a distance beyond which
%     TrackMate's Simple LAP Tracker will not attempt to link two spots. It
%     is probably accurate only to 0.1 microns.
% 
%  minimumTrackLength - This parameter sets the length below which tracks
%     are thrown out. This parameter MUST BE AN INTEGER; otherwise, you
%     have bebugged your minimum track length.
%
% Potential problems:
% This function requires FIJI to be present in the applications folder as
% FIJI.app. Any problems within TrackMate, FIJI, or your image will result
% in a full crash of the function.

function [ ] = trackmate_xml_creator( inFile, outFile, varargin )

if nargin < 3
    spotSize = 0.3;
else
    spotSize = varargin{1};
end
if nargin < 4
    baseline = 0.012;
else
    baseline = varargin{2};
end
if nargin < 5
    qualThresh = 0.00666666667;
else
    qualThresh = varargin{3};
end
if nargin < 6
    minTrackLength = 5;
else
    minTrackLength = varargin{4};
end
if nargin < 7
    linkDist = 0.0813;
else
    linkDist = varargin{5};
end

javaaddpath '/Applications/Fiji.app/jars/ij-1.50e.jar';
addpath('/Applications/Fiji.app/scripts');
Miji(false);
imp = ij.ImagePlus(inFile);
model = fiji.plugin.trackmate.Model();
model.setPhysicalUnits(imp.getCalibration().getUnits(), imp.getCalibration().getTimeUnit());
settings = fiji.plugin.trackmate.Settings();
settings.setFrom(imp)
settings.detectorFactory = fiji.plugin.trackmate.detection.LogDetectorFactory();
map = java.util.HashMap();
map.put('DO_SUBPIXEL_LOCALIZATION', true);
map.put('RADIUS', spotSize/2);
map.put('TARGET_CHANNEL', 1);
map.put('THRESHOLD', baseline);

map.put('DO_MEDIAN_FILTERING', false);

settings.detectorSettings = map;

filter1 = fiji.plugin.trackmate.features.FeatureFilter('QUALITY', qualThresh, true);

settings.addSpotFilter(filter1)

settings.trackerFactory = fiji.plugin.trackmate.tracking.oldlap.LAPTrackerFactory();

settings.trackerSettings = fiji.plugin.trackmate.tracking.LAPUtils.getDefaultLAPSettingsMap();

settings.trackerSettings.put('ALLOW_GAP_CLOSING', false);

settings.trackerSettings.put('GAP_CLOSING_MAX_DISTANCE', 0);

settings.trackerSettings.put('MAX_FRAME_GAP', java.lang.Integer(0));

settings.trackerSettings.put('LINKING_MAX_DISTANCE', linkDist);

settings.addTrackAnalyzer(fiji.plugin.trackmate.features.track.TrackDurationAnalyzer())

filter2 = fiji.plugin.trackmate.features.FeatureFilter('TRACK_DURATION', minTrackLength, true);

settings.addTrackFilter(filter2)



trackmate = fiji.plugin.trackmate.TrackMate(model, settings);

ok = trackmate.checkInput();

if ~ok

    display(trackmate.getErrorMessage())

end

ok = trackmate.process();

if ~ok

    display(trackmate.getErrorMessage())

end

myFile = outFile;

import java.util.Comparator;

import java.util.Date;

import org.jdom.Document;

import org.jdom.Element;

import org.jdom.output.XMLOutputter;

import java.util.TreeSet;

import org.jdom.output.Format;

import java.io.File;

import java.io.FileOutputStream;

trackModel = model.getTrackModel();

d = Date();

content = Element('Tracks');

content.setAttribute('nTracks', int2str(model.getTrackModel().nTracks(true)));

content.setAttribute('spaceUnits', model.getSpaceUnits());


content.setAttribute('frameInterval', sprintf('%.5f',settings.dt));

content.setAttribute('timeUnits', model.getTimeUnits());

content.setAttribute('generationDateTime', d.toString());

content.setAttribute('from', strcat(char(trackmate.PLUGIN_NAME_STR), ' v', char(trackmate.PLUGIN_NAME_VERSION)));

myData = trackModel.trackIDs(true);

mdi = myData.iterator();

for i = 1:trackModel.nTracks(true);

    trackElement = Element('particle');

    trackindex = java.lang.Integer(mdi.next());

    track = trackModel.trackSpots(trackindex);

    trackElement.setAttribute('nSpots',int2str(track.size()));

    ti = track.iterator();

    spotArray = zeros(track.size(),4);

    for j = 1:track.size()

        spot = ti.next();

        spotArray(j,1) = spot.getFeature(java.lang.String('FRAME'));

        spotArray(j,2) = spot.getFeature(java.lang.String('POSITION_X'));

        spotArray(j,3) = spot.getFeature(java.lang.String('POSITION_Y'));

        spotArray(j,4) = spot.getFeature(java.lang.String('POSITION_Z'));

    end

    spotArray = sortrows(spotArray);

    for j = 1:track.size()

        spotElement = Element('detection');

        spotElement.setAttribute('t',int2str(spotArray(j,1)));

        spotElement.setAttribute('x',sprintf('%.14f', spotArray(j,2)));

        spotElement.setAttribute('y',sprintf('%.14f', spotArray(j,3)));

        spotElement.setAttribute('z',sprintf('%.14f', spotArray(j,4)));

        trackElement.addContent(spotElement);

    end

    content.addContent(trackElement);

end

document = Document(content);

outputter = XMLOutputter(Format.getPrettyFormat());

file = File(myFile);

fos = FileOutputStream(file);

outputter.output(document, fos);
MIJ.exit;
fclose('all');

return