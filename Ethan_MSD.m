function [ MSD_timestep, MSD_D, MSD_V, MSD_alpha, Rsq_log_log_MSD] = Ethan_MSD( positionmat, pixelsPerMicron )
%Ethan_MSD 
%   Given a set of XY coordinates, calculates the MSD vs time,
%   and extracts D, V, alpha, and also provides the Rsq fit of a
%   line to the log-log MSD vs time.

%% Ethan's code
globals.MsdFitOptions = fitoptions('Method','NonlinearLeastSquares','Lower',[0 0],'Upper',[Inf Inf],'Startpoint',[0 0]);
globals.MsdFitOptions_NB = fitoptions('Method','NonlinearLeastSquares','Startpoint',[0 0]);
globals.debugtrace=0;

tmp.ACmeanrsquare=[];
tmp.Xvals=positionmat(:,1);
tmp.Yvals=positionmat(:,2);
tmp.XvalsTransp=transpose(tmp.Xvals);
tmp.YvalsTransp=transpose(tmp.Yvals);

tmp.lifeX=size(positionmat,1);
tmp.end_m=tmp.lifeX*(0.8);

tmp.roundcheck=tmp.end_m-round(tmp.end_m);
if (tmp.roundcheck < 0.5 )
    tmp.end_m=round(tmp.end_m);
else
    tmp.end_m=round(tmp.end_m)-1;
end

tmp.ACmeanrsquare=[];

checkdt=1;
for dt = 1:(tmp.end_m-1)
    tmp.ar1_end=tmp.end_m-dt;
    tmp.diffxA = tmp.XvalsTransp(1:tmp.ar1_end) - tmp.XvalsTransp((1+dt):(tmp.end_m));
    tmp.diffyA = tmp.YvalsTransp(1:tmp.ar1_end) - tmp.YvalsTransp((1+dt):(tmp.end_m));
    tmp.ACrsquare = tmp.diffxA.*tmp.diffxA + tmp.diffyA.*tmp.diffyA;
    tmp.ACmeanrsquare(dt) = mean(tmp.ACrsquare);
    checkdt=dt;
end

MSD_timestep =tmp.ACmeanrsquare;
tmp.x2i=transpose(1:tmp.end_m-1);
tmp.y2i=transpose(tmp.ACmeanrsquare(1:tmp.end_m-1));
tmp.x2i(isnan(tmp.x2i)) = [];
tmp.y2i(isnan(tmp.y2i)) = [];
tmp.y2i=double(tmp.y2i);

%Fit MSD to a curve.
tmp.fMsd = fittype('4*A*x+(B*x)^2','options', globals.MsdFitOptions);
tmp.OMsd = fit(tmp.x2i,tmp.y2i,tmp.fMsd);
tmp.yOfit = 4*tmp.OMsd.A*tmp.x2i+(tmp.OMsd.B*tmp.x2i).^2;
MSD_D =tmp.OMsd.A;                    %diffusion componglobals.MsdFitOptionsent of fit (bounded at 0).
MSD_V =tmp.OMsd.B*(1000/pixelsPerMicron);                    %Velocity component of fit  (bounded at 0).

tmp.X_MSD=log10(1:(tmp.end_m-1));
tmp.Y_MSD=log10(tmp.ACmeanrsquare(1:(tmp.end_m-1)));
tmp.X_MSD(isnan(tmp.X_MSD)) = [];
tmp.Y_MSD(isnan(tmp.Y_MSD)) = [];

%tmp.pMsd=scatter(tmp.X_MSD,tmp.Y_MSD);
%get slope of line
%tmp.hMsd=lsline;

tmp.p2Msd = polyfit(tmp.X_MSD,tmp.Y_MSD,1);
tmp.sMsd=tmp.p2Msd(1);
MSD_alpha = tmp.sMsd;

%get Rsquared of line.
tmp.ypredM = polyval(tmp.p2Msd,tmp.X_MSD);          % predictions
tmp.RdevM = tmp.Y_MSD - mean(tmp.Y_MSD);            % deviations - measure of spread
tmp.SSTM = sum(tmp.RdevM.^2);                   % total variation to be accounted for
tmp.residM = (tmp.Y_MSD) - tmp.ypredM;              % residuals - measure of mismatch
tmp.SSEM = sum(tmp.residM.^2);                  % variation NOT accounted for
tmp.Rsq_log_log_MSD = 1 - tmp.SSEM/tmp.SSTM;        % percent of error explained
Rsq_log_log_MSD=tmp.Rsq_log_log_MSD;





end

