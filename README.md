# README #

Files to use:

trackmate_xml_creator runs TrackMate on a TIF file. The utility of doing this in MATLAB and looping over files or parameters should be obvious.

NOTE/WARNING/DANGER: A version of TrackMate older than v3 is required. Also, you may need to change your system-wide limit on the number of files open to avoid crashes. You may also need to change the javaaddpath section of the code to reflect the path to your ImageJ.

track_properties_analyzer uses pill-meshed cell outlines derived from Morphometrics (by Tristan Ursell) to determine properties of tracks that were created using trackmate_xml_creator.

aggregate_analyzer is a crappy iterator of track_properties_analyzer that requires manually entering the files to loop over in the manner of the example code already typed in. It is present in case it can save you the work of extracting a few properties from the object created by track_properties_analyzer.