function [ Rfit ] = Saman_PCA_Rfit( positionmat )
%Saman_PCA_Rfit 

%     Orthogonal regression using PCA (adapted from Saman)
%     Gives an Rsq value between 0.5 and 1, which represents how
%     well the trajectory can be fit to a line.

[coeff, score, roots] = pca(positionmat);
%     basis = coeff(:,1);
%     normal = coeff(:,2);
pctExplained = roots' ./ sum(roots);
Rfit = pctExplained(1);
%         Unused code to get the orientation of the tracks below: 

%         meanpos = mean(positionmat,1);
%         [row col] = size(positionmat);
%         Xfit = repmat(meanpos,row,1) + score(:,1)*coeff(:,1)';
%         %store slope and angle.
%         theta = atand((Xfit(1,2)-Xfit(2,2))/(Xfit(1,1)-Xfit(2,1)));
%         X(i).theta=theta;
%         X(i).linefit = Xfit(:,:);

end

