stringList = { ...
'eMK36/1be8_tracks.xml';
'eMK36/2be8_tracks.xml';
'eMK36/3be8_tracks.xml';
'eMK36/1-eMK36-phase-bordered_19-Mar-2016_CONTOURS_pill_MESH.mat';
'eMK36/2-eMK36-phase-bordered_19-Mar-2016_CONTOURS_pill_MESH.mat';
'eMK36/3-eMK36-phase-bordered_19-Mar-2016_CONTOURS_pill_MESH.mat';
};
cellWidthCutoff = 4000;

nofiles = size(stringList,1)/2;
allTracks = [];
for i=1:nofiles
    a = track_properties_analyzer_mrinal( stringList{i}, stringList{i+nofiles}, 5, cellWidthCutoff );
    allTracks = [allTracks, a];        
end
clear a

notracks = size(allTracks,2);
tracksOrs = zeros(notracks,3);

for i = 1:notracks
    tracksOrs(i,1) = allTracks(i).midline_angle;
    tracksOrs(i,2) = allTracks(i).speed;
    tracksOrs(i,3) = allTracks(i).alpha;
    tracksOrs(i,4) = allTracks(i).MSD_V;
    tracksOrs(i,5) = allTracks(i).MSD_alpha;
    tracksOrs(i,6) = allTracks(i).Rsq_log_log_MSD;
end

tracks_in_cells = tracksOrs;
%clear tracksOrs notracks
clear nofiles
a=tracks_in_cells(:,4)>0.01;
b=tracks_in_cells(:,6)>0.9;
c = a.*b;
short_tracks = tracks_in_cells(logical(c),:);